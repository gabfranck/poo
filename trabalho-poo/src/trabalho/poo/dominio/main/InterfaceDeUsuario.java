package trabalho.poo.dominio.main;

import java.util.Scanner;

public class InterfaceDeUsuario {
	
	public void menuInicial() {
		Scanner sc = new Scanner(System.in);
		int in;
		int res;
		System.out.println("Bem-vindo ao sistema, por favor indentifique-se:");
		System.out.println("1 - Secretária");
		System.out.println("2 - Médico");
		in = sc.nextInt();
		
		switch (in) {
			case 1: {
				System.out.println(1);
				break;
			}
			case 2: {
				System.out.println(2);
				break;
			}
			default: {
				System.out.println("Opção inválida!!");
				menuInicial();
			}
			
		}
			
			
	}

}
