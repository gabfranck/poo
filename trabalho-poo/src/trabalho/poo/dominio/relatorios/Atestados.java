package trabalho.poo.dominio.relatorios;

import java.util.ArrayList;
import java.util.List;

import trabalho.poo.dominio.pessoas.Medico;
import trabalho.poo.dominio.pessoas.Paciente;
import trabalho.poo.dominio.procedimentos.Consulta;
import trabalho.poo.dominio.procedimentos.Prontuario;

public class Atestados extends RelatorioGeral {
	
	private Consulta consulta;
	
	private int dias;

	public Atestados(Consulta consulta, int dias) {
		super();
		this.consulta = consulta;
		this.dias = dias;
	}

	@Override
	public void gerarRelatorio() {
		System.out.println("ATESTADO MÉDICO");
		System.out.println("");
		System.out.println("Atesto para os devidos fins, a pedido, que o(a) Sr(a) " + consulta.getPaciente().getNome() + " " + consulta.getPaciente().getSobreNome());
		System.out.println("inscrito no CPF sob o número  " + consulta.getPaciente().getCpf() + ", paciente sob os meus cuidados,");
		System.out.println("não se encontra em condições para o trabalho, devendo seu afastamento");
		System.out.println("ser considerado pela quantidade dias " + dias + " a partir da data deste documento.");
		System.out.println("");
		System.out.println("");
		System.out.println("Día:" + dia.getDayOfMonth() + "/" + dia.getMonthValue() + "/" + dia.getYear());
		System.out.println("Hora: " + dia.getHour() + ":" + dia.getMinute());
		System.out.println("");
		System.out.println("");
		System.out.println("                              " + "Dr.: " +  consulta.getMedico().getNome() + " " + consulta.getMedico().getSobreNome());
		System.out.println("                              " + "CRM: " + " " + consulta.getMedico().getCRM());
	}
	
	public static void main(String[] args) {
		Medico m = new Medico();
		Paciente p = new Paciente();
		Consulta c =  new Consulta();
		Prontuario pr = new Prontuario();
		
		m.setNome("House");
		m.setSobreNome("");
		m.setCRM("CP200");
		
		p.setNome("Cara");
		p.setSobreNome("da caneta blue");
		p.setCpf("078.048.311-10");
		
		List<String> sintomas = new ArrayList<>();
		sintomas.add("Dor de cabeça");
		sintomas.add("Náuseas");
		sintomas.add("Vómito");
		
		pr.setSintomas(sintomas);
		
		c.setMedico(m);
		c.setPaciente(p);
		c.setProntuario(pr);
		
		Atestados a = new Atestados(c,5);
		
		a.gerarRelatorio();
		
	}
	
}
