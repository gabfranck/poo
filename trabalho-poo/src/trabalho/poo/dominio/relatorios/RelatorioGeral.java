package trabalho.poo.dominio.relatorios;

import java.time.LocalDateTime;

public abstract class RelatorioGeral {
	
	 protected LocalDateTime dia =LocalDateTime.now();
	 
	 public abstract void gerarRelatorio();


}
