package trabalho.poo.dominio.relatorios;

import java.util.ArrayList;
import java.util.List;

import trabalho.poo.dominio.pessoas.Medico;
import trabalho.poo.dominio.pessoas.Paciente;
import trabalho.poo.dominio.procedimentos.Consulta;
import trabalho.poo.dominio.procedimentos.Prontuario;

public class Receitas extends RelatorioGeral {
	
	private Consulta consulta;
	
	private String obs;
	
	private String encaminhamento;
	
	public Receitas(Consulta consulta, String obs, String encaminhamento) {
		super();
		this.consulta = consulta;
		this.obs = obs;
		this.encaminhamento = encaminhamento;
	}

	@Override
	public void gerarRelatorio() {
		System.out.println("CENTRO MÉDICO");
		System.out.println("");
		System.out.println("Paciente: " + consulta.getPaciente().getNome() + " " + consulta.getPaciente().getSobreNome());
		System.out.println("CPF: " + consulta.getPaciente().getCpf());
		System.out.println("Telefóne: "+ consulta.getPaciente().getTelefone());
		System.out.println("Endereço: "+ consulta.getPaciente().getEndereco());
		System.out.println("E-mail: "+ consulta.getPaciente().getEmail());
		System.out.println("");
		System.out.println("");
		System.out.println("Médicamentos: ");
		System.out.println("");
		for(String prescricao : consulta.getProntuario().getPrescricoes()) {
			System.out.println("->" + prescricao);
		}
		System.out.println("");
		System.out.println("");
		System.out.println("Obs:");
		System.out.println(obs);
		System.out.println("");
		System.out.println("");
		System.out.println("Encaminhamento:");
		System.out.println(encaminhamento);
		System.out.println("");
		System.out.println("");
		System.out.println("                              " + "Dr.: " +  consulta.getMedico().getNome() + " " + consulta.getMedico().getSobreNome());
		System.out.println("                              " + "CRM: " + " " + consulta.getMedico().getCRM());
	}
	
	public static void main(String[] args) {
		Medico m = new Medico();
		Paciente p = new Paciente();
		Consulta c =  new Consulta();
		Prontuario pr = new Prontuario();
		
		m.setNome("House");
		m.setSobreNome("");
		m.setCRM("CP200");
		
		p.setNome("Cara");
		p.setSobreNome("da caneta blue");
		p.setCpf("078.048.311-10");
		p.setTelefone("44 9983 41377");
		p.setEmail("menossiv@gmail.com");
		p.setEndereco("Naquela rua alí embaixo");
		
		List<String> sintomas = new ArrayList<>();
		sintomas.add("Dor de cabeça");
		sintomas.add("Náuseas");
		sintomas.add("Vómito");
		
		List<String> prescricao = new ArrayList<>();
		prescricao.add("Novalgina em gotas");
		prescricao.add("Ketorolac 500mg");
		prescricao.add("Comprensa fria na bunda");
		
		pr.setSintomas(sintomas);
		pr.setPrescricoes(prescricao);
		
		c.setMedico(m);
		c.setPaciente(p);
		c.setProntuario(pr);
		
		Receitas r = new Receitas(c,"Nenhuma observacao", "Encaminhamento para o manicómio mais próximo.");
		
		r.gerarRelatorio();
	}

}
