package trabalho.poo.dominio.pessoas;

import trabalho.poo.dominio.procedimentos.DadosAdicionais;

public class Paciente extends Pessoa {
	
	public Paciente() {
		super();
	}
	
	

	public Paciente(Integer id, TipoConvenio tipoConvenio, DadosAdicionais dadosAdicionais) {
		super();
		this.id = id;
		this.tipoConvenio = tipoConvenio;
		this.dadosAdicionais = dadosAdicionais;
	}



	private Integer id;
	
	private TipoConvenio tipoConvenio;
	
	private DadosAdicionais dadosAdicionais;
	
	
	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public TipoConvenio getTipoConvenio() {
		return tipoConvenio;
	}



	public void setTipoConvenio(TipoConvenio tipoConvenio) {
		this.tipoConvenio = tipoConvenio;
	}



	public enum TipoConvenio {
		PARTICULAR,
		PLANO_SAUDE
	}



	public DadosAdicionais getDadosAdicionais() {
		return dadosAdicionais;
	}



	public void setDadosAdicionais(DadosAdicionais dadosAdicionais) {
		this.dadosAdicionais = dadosAdicionais;
	}
	
	

}
