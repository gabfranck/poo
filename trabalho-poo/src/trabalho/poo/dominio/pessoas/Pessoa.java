package trabalho.poo.dominio.pessoas;

import java.util.Date;

public abstract class Pessoa {
	
	public Pessoa() {}
	
	public Pessoa(Integer id, String cpf, String nome, String sobreNome, Date nascimento, String endereco,
			String telefone, String email) {
		super();
		this.id = id;
		this.cpf = cpf;
		this.nome = nome;
		this.sobreNome = sobreNome;
		this.nascimento = nascimento;
		this.endereco = endereco;
		this.telefone = telefone;
		this.email = email;
	}

	private Integer id;
	
	private String cpf;
	
	private String nome;
	
	private String sobreNome;
	
	private Date nascimento;
	
	private String endereco;
	
	private String telefone;
	
	private String email;	
	
	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getCpf() {
		return cpf;
	}



	public void setCpf(String cpf) {
		this.cpf = cpf;
	}



	public String getNome() {
		return nome;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}



	public String getSobreNome() {
		return sobreNome;
	}



	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}



	public Date getNascimento() {
		return nascimento;
	}



	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}



	public String getEndereco() {
		return endereco;
	}



	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}



	public String getTelefone() {
		return telefone;
	}



	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}

}
