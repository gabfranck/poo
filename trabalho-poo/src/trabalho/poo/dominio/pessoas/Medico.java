package trabalho.poo.dominio.pessoas;

public class Medico extends Pessoa {

	public Medico() {
		super();
	}	
	
	public Medico(Integer id, String especialidade, String cRM) {
		super();
		this.id = id;
		this.especialidade = especialidade;
		CRM = cRM;
	}



	private Integer id;
	
	private String especialidade;
	
	private String CRM;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public String getCRM() {
		return CRM;
	}

	public void setCRM(String cRM) {
		CRM = cRM;
	}
	
	
	
	

}
