package trabalho.poo.dominio.procedimentos;

import java.time.LocalDateTime;

import trabalho.poo.dominio.pessoas.Medico;
import trabalho.poo.dominio.pessoas.Paciente;

public class Consulta {
	
	public Consulta() {
	}

	public Consulta(Integer id, LocalDateTime data, Medico medico, Paciente paciente, Tipo tipo,
			Prontuario prontuario) {
		super();
		this.id = id;
		this.data = data;
		this.medico = medico;
		this.paciente = paciente;
		this.tipo = tipo;
		this.prontuario = prontuario;
	}

	private Integer id;
	
	private LocalDateTime data = LocalDateTime.now();
	
	private Medico medico;
	
	private Paciente paciente;
	
	private Tipo tipo;
	
	private Prontuario prontuario;
	
	private enum Tipo{
		NORMAL,
		RETORNO
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getData() {
		return data;
	}
	
	public String getDataFormated() {
		int dia = data.getDayOfMonth();
		int mes = data.getMonthValue();
		int ano = data.getYear();
		
		String data = dia + "/" + mes + "/" + ano;
		
		return data;
		
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	
	
	public static void main(String[] args) {
		Consulta c = new Consulta();
		
		System.out.println(c.getData());
		System.out.println(c.getDataFormated());
	}

	public Prontuario getProntuario() {
		return prontuario;
	}

	public void setProntuario(Prontuario prontuario) {
		this.prontuario = prontuario;
	}
	
	
	
	

}
