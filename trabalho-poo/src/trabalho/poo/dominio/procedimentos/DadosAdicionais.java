package trabalho.poo.dominio.procedimentos;

import java.util.List;

import trabalho.poo.dominio.pessoas.Paciente;

public class DadosAdicionais {
	
	public DadosAdicionais() {
		
	}
		
	public DadosAdicionais(Integer id, Paciente paciente, boolean fuma, boolean bebe, boolean colesterol,
			boolean diabete, boolean doencaCardiaca, List<String> cirurgias, List<String> alergias) {
		super();
		this.id = id;
		this.paciente = paciente;
		this.fuma = fuma;
		this.bebe = bebe;
		this.colesterol = colesterol;
		this.diabete = diabete;
		this.doencaCardiaca = doencaCardiaca;
		this.cirurgias = cirurgias;
		this.alergias = alergias;
	}

	private Integer id;
	
	private Paciente paciente;
	
	private boolean fuma;
	
	private boolean bebe;

	private boolean colesterol;

	private boolean diabete;

	private boolean doencaCardiaca;
	
	private List<String> cirurgias;
	
	private List<String> alergias;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public boolean isFuma() {
		return fuma;
	}

	public void setFuma(boolean fuma) {
		this.fuma = fuma;
	}

	public boolean isBebe() {
		return bebe;
	}

	public void setBebe(boolean bebe) {
		this.bebe = bebe;
	}

	public boolean isColesterol() {
		return colesterol;
	}

	public void setColesterol(boolean colesterol) {
		this.colesterol = colesterol;
	}

	public boolean isDiabete() {
		return diabete;
	}

	public void setDiabete(boolean diabete) {
		this.diabete = diabete;
	}

	public boolean isDoencaCardiaca() {
		return doencaCardiaca;
	}

	public void setDoencaCardiaca(boolean doencaCardiaca) {
		this.doencaCardiaca = doencaCardiaca;
	}

	public List<String> getCirurgias() {
		return cirurgias;
	}

	public void setCirurgias(List<String> cirurgias) {
		this.cirurgias = cirurgias;
	}

	public List<String> getAlergias() {
		return alergias;
	}

	public void setAlergias(List<String> alergias) {
		this.alergias = alergias;
	}
	
}
